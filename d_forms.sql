-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 19, 2021 at 12:32 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `form_builder`
--

-- --------------------------------------------------------

--
-- Table structure for table `d_forms`
--

CREATE TABLE `d_forms` (
  `id` int(11) NOT NULL,
  `form_name` varchar(50) NOT NULL,
  `form_code` varchar(50) NOT NULL,
  `form_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `d_forms`
--

INSERT INTO `d_forms` (`id`, `form_name`, `form_code`, `form_data`) VALUES
(5, 'CONTACT FORM', 'contact_form', '\r\n\r\n[{\"field_name\":\"name\",\"field_type\":\"textbox\",\"is_required\":\"yes\",\"field_opt\":0},{\"field_name\":\"email\",\"field_type\":\"textbox\",\"is_required\":\"yes\",\"field_opt\":0},{\"field_name\":\"description\",\"field_type\":\"textarea\",\"is_required\":\"yes\",\"field_opt\":0}]'),
(6, 'USER DETAILS', 'user_details', '[{\"field_name\":\"name\",\"field_type\":\"textbox\",\"is_required\":\"yes\",\"field_opt\":0},{\"field_name\":\"email\",\"field_type\":\"textbox\",\"is_required\":\"yes\",\"field_opt\":0},{\"field_name\":\"gender\",\"field_type\":\"radio\",\"is_required\":\"yes\",\"field_opt\":[\"male\",\"female\"]},{\"field_name\":\"education\",\"field_type\":\"checkbox\",\"is_required\":\"yes\",\"field_opt\":[\"ssc\",\"hsc\",\"graduate\",\"post-graduate\"]},{\"field_name\":\"marital status\",\"field_type\":\"select\",\"is_required\":\"yes\",\"field_opt\":[\"married\",\"unmarried\"]},{\"field_name\":\"description\",\"field_type\":\"textarea\",\"is_required\":\"yes\",\"field_opt\":0}]'),
(8, 'STUDENT DETAILS', 'student_details', '[{\"field_name\":\"name\",\"field_type\":\"textbox\",\"is_required\":\"yes\",\"field_opt\":0},{\"field_name\":\"email\",\"field_type\":\"textbox\",\"is_required\":\"yes\",\"field_opt\":0},{\"field_name\":\"gender\",\"field_type\":\"radio\",\"is_required\":\"yes\",\"field_opt\":[\"male\",\"female\"]},{\"field_name\":\"education\",\"field_type\":\"checkbox\",\"is_required\":\"yes\",\"field_opt\":[\"ssc\",\"hsc\",\"graduate\",\"post-graduate\"]},{\"field_name\":\"description\",\"field_type\":\"textarea\",\"is_required\":\"yes\",\"field_opt\":0}]');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `d_forms`
--
ALTER TABLE `d_forms`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `d_forms`
--
ALTER TABLE `d_forms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
