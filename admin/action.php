<?php 

	include_once('includes/class.form.php');

	$dbObj = new Form();

	if (isset($_POST['action']) && $_POST['action'] == "check_name") {
		$form_name = $_POST['form_name'];
		echo $dbObj->check_name($form_name);
	}

	if (isset($_POST['action']) && $_POST['action'] == "insert") {

		$form_name = $_POST['form_name'];
		$form_code = $_POST['form_code'];
		
		$field_name = $_POST['field_name'];
		$field_type = $_POST['field_type'];
		$field_required = $_POST['field_required'];

		
		
		$test_field[] = array("field_name"=>'name','field_type'=>'textbox','is_required'=>'yes','field_opt'=>0);
		$test_field[] = array("field_name"=>'email','field_type'=>'textbox','is_required'=>'yes','field_opt'=>0);
		$test_field[] = array("field_name"=>'gender','field_type'=>'radio','is_required'=>'yes','field_opt'=>array('male','female'));

		$test_field[] = array("field_name"=>'education','field_type'=>'checkbox','is_required'=>'yes','field_opt'=>array('ssc','hsc','graduate','post-graduate'));

		$test_field[] = array("field_name"=>'marital status','field_type'=>'select','is_required'=>'yes','field_opt'=>array('married','unmarried'));

		$test_field[] = array("field_name"=>'description','field_type'=>'textarea','is_required'=>'yes','field_opt'=>0);
// 		
		
		print_r(json_encode($test_field));
		
		exit;
		
		echo $dbObj->insertRecond($form_name, $form_code, json_encode($field_arr));
	}
	if (isset($_POST['action']) && $_POST['action'] == "display") {		
		echo $dbObj->displayRecond();
	}
?>