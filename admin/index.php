<html>
	<head>
		<title>Form Builder</title>
		<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
		<link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<style type="text/css">
			#formCodeLbl{
				display: block;
				font-weight: normal;
			    width: 100%;
			    height: 34px;
			    padding: 6px 12px;
			    font-size: 14px;
			    line-height: 1.42857143;
			    color: #555;
			    background-color: #fff;
			    background-image: none;
			    border: 1px solid #ccc;
			    border-radius: 4px;
			    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
			    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
			    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
			    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
			    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
}


			}
		</style>
	</head>
	<body>
		<h2 style="text-align: center;">Create New Form</h2>
		
		<div class="container">		
			<hr>	
		
			<form id="formBuilder" action="./form_code.php" method="post">
			  <div class="form-row">
			    <div class="form-group col-md-6">
			      <label for="formName">Form Name</label>
			      <input type="text" name="form_name" class="form-control" id="formName" placeholder="Form Name">
			    </div>
			    <div class="form-group col-md-6">
			      <label for="formCode">Form Code</label>
			      <label for="formCode" id="formCodeLbl">Form Code</label>
			      <input type="hidden" name="form_code" class="form-control" id="formCode" placeholder="Form Code">

			    </div>
			  </div>

			  <div class="form-row clone-fields">
			    <div class="form-group col-md-4">
			      <label for="fieldName">Field Name</label>
			      <input type="text" name="field_name[]" class="form-control" id="fieldName" placeholder="Field Name">
			    </div>	
			    <div class="form-group col-md-3">
			      <label for="fieldType">Field Type</label>
			      <select name="field_type[]" id="fieldType" class="form-control">
			      		<option>---Select---</option>
			      		<option value="textbox">Textbox</option>
			      		<option value="textarea">Textarea</option>
			      </select>
			    </div>
			    <div class="form-group col-md-3">
			      <label for="fieldRequired">Field Required</label>
			      <select name="field_required[]" id="fieldRequired" class="form-control">
			      		<option>---Select---</option>
			      		<option value="yes">Yes</option>
			      		<option value="no">No</option>
			      </select>			      
			    </div>		
			    <div class="form-group col-md-2 btn-class">
			    	<label style="height: 40px;"></label>
			    	<button id="addNewField" class="btn btn-primary">Add</button>
			    </div>    
			  </div>		  

			  <div class="form-row">
			    <div class="form-group col-md-12">
			      <label></label>
			      <button type="submit" class="btn btn-primary">Submit</button>
			    </div>			    
			  </div>	

			  
			</form>
		</div>

		<hr>
		<center><h2>Forms</h2></center>

		<div class="container mt-5" id="get">
			<table id="forData">
			    <thead>
			      <tr>
			          <th>Id</th>
			          <th>Form Name</th>
			          <th>Form Code</th>
			      </tr>
			    </thead>
			</table>
		</div>

	
	<script type="text/javascript" src="../js/jquery.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script src="//cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>	
	<script type="text/javascript">
		$(document).ready(function(){

			$('#addNewField').click(function(e){
				e.preventDefault();
			
				var newele = $('.clone-fields:last').clone();
				newele.find(".btn-class").remove();
				newele.find("input").val("");
			
			  $(newele).insertAfter(".clone-fields:last");
			});

			$("#formName").blur(function () {
	            var formName = $(this).val($(this).val().toUpperCase());

	            $.ajax({
			        type: "POST",
			        url: "action.php",
			        data: "form_name="+$(this).val()+"&action=check_name",
			        success: function(responce)
			        {
			        	if(responce == true){
			          		alert("This Form Name is alredy Available. Please Use Another Name");
			            }
			            else{
			            	var form_code = $(formName).val().toLowerCase();
							form_code = form_code.replace(/\s+/g, '_');
							$("#formCodeLbl").text(form_code);
							$("#formCode").val(form_code);
							
			            }
			        }
			    });
	        });

			$("#formBuilder").submit(function(e) {
			    e.preventDefault();

			    $.ajax({
			        type: "POST",
			        url: "action.php",
			        data: $("#formBuilder").serialize()+"&action=insert",
			        success: function(responce)
			        {
			            if(responce == true){
			          		alert("Form Creation successfully");
			           		location.reload();
			            }else{
			           		alert("Form Creation Failed");
			            }
			        }
			    });
			});

			$.ajax({
			    type: "POST",
			    url: "action.php",
			    dataType: "json",
			    data: "action=display",
			    success: function(data)
			    {
			 		$("#forData").dataTable({
                        data: data,
                        sort: true,
                        searching: true,
                        paging: true,
                        columns: [
                            {'data': 'row_cnt'},
                            {'data': 'form_name'},
                            {'data': 'form_code'},
                        ]
                    });
                  }
			    });
		});
	</script>

	</body>
</html>
