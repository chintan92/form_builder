<?php
	
	class Generation
	{
		private $servername = "localhost";
		private $username   = "root";
		private $password   = "";
		private $dbname = "form_builder";
		public $con;
		public $table = "d_forms";

		public function __construct()
		{
			try {
				$this->con = new mysqli($this->servername, $this->username, $this->password, $this->dbname);	
			} catch (Exception $e) {
				echo $e->getMessage();
			}
		}

		public function form($form_code)
		{
			$sql = "SELECT form_name,form_data FROM $this->table where form_code='$form_code'";
			$result = $this->con->query($sql);

			function stdToArray($obj){
			  $reaged = (array)$obj;
			  foreach($reaged as $key => &$field){
			    if(is_object($field))$field = stdToArray($field);
			  }
			  return $reaged;
			}

			if ($result->num_rows > 0) {

				$row = $result->fetch_assoc();				
				$form_attr = preg_replace('/\s+/', '_', strtolower($row['form_name']));

				$form = '<h2 style="text-align: center;">'.$row['form_name'].'</h2>';
				$form .= '<div class="container">		
							<hr>			
							<form name="'.$form_attr.'" method="post">';
							$fields = json_decode($row['form_data']);
								$array = array();
								foreach($fields as $key=>$value){
								    $array[$key] = stdToArray($value);
								}
								
								foreach($array as $key=>$value){
									$required = "";
										if($value['is_required'] == "yes"){
											$required = "required";
										}
									$name_attr = preg_replace('/\s+/', '_', strtolower($value['field_name']));

									if($value['field_type'] == "textarea"){
										$form .='<div class="form-row">
									    <div class="form-group col-md-12">
									      <label>'.ucwords($value['field_name']).'</label>
									      <textarea name="'.$name_attr.'" class="form-control" style="height:200px"></textarea>
									    </div>							    
									  </div>';
										
									}elseif ($value['field_type'] == "radio") {
										
										$form .='<div class="form-row">
									    <div class="form-group col-md-12">
									      <label>'.ucwords($value['field_name']).'</label><br>';

									      foreach ($value['field_opt'] as $key => $opt_value) {
											$form .='<input type="radio" name="'.$name_attr.'[]" '.$required.' value='.$opt_value.'>'.'&ensp;<label for="genderm">'.$opt_value.'</label>&ensp;';
										}								     

									    $form .='</div>							    
									  </div>';
									}elseif ($value['field_type'] == "checkbox") {
										
										$form .='<div class="form-row">
									    <div class="form-group col-md-12">
									      <label>'.ucwords($value['field_name']).'</label><br>';

									      foreach ($value['field_opt'] as $key => $opt_value) {
											$form .='<input type="checkbox" name="'.$name_attr.'[]" value='.$opt_value.'>'.'&ensp;<label for="genderm">'.$opt_value.'</label>&ensp;';
										}								     

									    $form .='</div>							    
									  </div>';
									}elseif ($value['field_type'] == "select") {
										
										$form .='<div class="form-row">
									    <div class="form-group col-md-12">
									      <label>'.ucwords($value['field_name']).'</label><br>';
									      $form .='<select name="'.$name_attr.'" class="form-control" '.$required.' >
									      		<option value="">Select</option>';
									      foreach ($value['field_opt'] as $key => $opt_value) {
									      	
									      		 $form .='<option value="'.$opt_value.'">'.ucwords($opt_value).'</option>';
											
										}								     
											$form .='</select>';
									    $form .='</div>							    
									  </div>';
									}
									else {
										
										$form .='<div class="form-row">
									    <div class="form-group col-md-12">
									      <label>'.ucwords($value['field_name']).'</label>
									      <input type="text" name="'.$name_attr.'" class="form-control" '.$required.'>
									    </div>							    
									  </div>';   
									}								 	
								}

							$form .='<div class="form-row">
							    <div class="form-group col-md-12">
							      <label></label>
							      <input type="hidden" id="cookieId" value="'.$form_attr.'">
							      <button type="submit" class="btn btn-primary">Submit</button>
							    </div>			    
							  </div>						  
							</form>
						</div>';

				return $form;

			}else{
				return false;
			}
		}
	}

	function do_shortcode($short_code){
		$dbObj = new Generation();
		print_r($dbObj->form($short_code));
	}
?>