<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css">
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
</head>
<body>
	<?php 
		include_once('form_generation/class.generation.php');
		// echo do_shortcode("contact_form");
		echo do_shortcode("user_details");
	?>

	<hr>
		<center><h2>Forms Details</h2></center>

		<div class="container mt-5" id="get">
			<table id="formData">
			   
			</table>
		</div>
	<script type="text/javascript" src="./js/jquery.min.js"></script>
	<script type="text/javascript" src="./js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
	<script src="//cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>	
	<script type="text/javascript">
		$(document).ready(function(){
			var cookie_name = $('form').find("#cookieId").val();
			if (document.cookie.indexOf(cookie_name) != -1){
				var form_data = $.parseJSON($.cookie(cookie_name));
			    var my_columns = [];
			    $.each( form_data[0], function( key, value ) {
				        var my_item = {};
				        my_item.data = key;
				        my_item.title = key;
				        my_columns.push(my_item);
				});

			   var table = $("#formData").dataTable({
	                data: form_data,
	    			"columns": my_columns
	            });
			}
		
		$("form").submit(function(e) {
			e.preventDefault();

			var data = $(this).serialize().split("&");
		    var data_obj={};
		    
		
		    for(var key in data)
		    {
		       data_obj[data[key].split("=")[0]] = data[key].split("=")[1];
		    }
		    
		    if (document.cookie.indexOf(cookie_name) != -1){
		    	var form_data = $.parseJSON($.cookie(cookie_name));
		    	form_data.push(data_obj);
				$.cookie(cookie_name, JSON.stringify(form_data));
		    }else{
		    	var data_array=[data_obj];
		    	$.cookie(cookie_name, JSON.stringify(data_array));  
		    }
		  

		});	
	});
	</script>
</body>
</html>